import pandas as pd
import numpy as np

def module_level_function(try1, try2):
    '''
    This fuction was intended to try to document a fuction outsid of a module

    :param try1: Does nothing
    :param try2:  Nothing again

    :return: Nothing
    '''

def module_level_function_without_documentation(self,columns_to_delete):
    print 'nothing'



class Cow(object):
    """
    This class was created to tidy toy cows in boxes. The class cow have the sub class :class:`cool_animals.Calf`. It is also similar to class :class:`not_cool_animals.Horses`
    def __init__(self, csv_file_directory,names, dtype ):
        """

    def rm_key_values(self, list_Key_values):
        """
        This method remove cows that are missing pieces

        :param list_Key_values: (list) List containing the names of the key colums to be checked.

        :return: The data frame without the cows missing key values

        Example:

        >>> rm_key_values(['Body weight','Age'])
        'Body weight  Age
            100        25
             80        32'
        """
        self.data_frame = self.data_frame.dropna(axis=0, how='any', subset=list_Key_values)

    def select_cows(self, list_col_criteria):
        """
        Remove cases that doesn't math at least one of the criteria given in the list_col_criteria.

        :param list_col_criteria: (list containing three object tuples) The fist object it's the column to apply the criteria, the second is the logical operator and the third is the threshold or target value. Note that the name of the column should have double quotation
                                 because will be given as argument to eval() through .format() and will loose a couple of quotes.

        :return: The data frame without the removed values

        Example of use:

        >>> select_cases([('"CUTE"', '>', '7'), ('"FUN"', '>', '30')]
        """
        for column in self.data_frame.columns:
            for col in list_col_criteria:
                if column in col[0]:
                    self.data_frame = eval("self.data_frame[(self.data_frame[{0[0]}] {0[1]} {0[2]})]".format(col))

    def no_docstrings(self,columns_to_delete):
        print "hello"




class Calf(Cow):
    """
    This class create calfs and is inherited form :class:`cool_animals.Cow`. So also contain the following methods. So,
    also can use this method :func:`for select some calves <cool_animals.Cow.select_cows>`
    """


    def add_HOUSA(self, columns):
        """
        This method add 'HOUSA' at the begining of the  selected columns

        :param columns: (list of columns) The content of the columns included in this list will be pasted to 'HOUSA'

        :return: The data frame with the selectd columns pasted to HOUSA

        EXAMPLE:

        >>> print dataframe
        'Age    Name
          20    jake'

        >>> add_HOUSA(['Age'])
        >>> print dataframe
        'Age         Name
          HOUSA20    jake'
        """

        for row in self.data_frame.index:
            for col in [columns]:
                if self.data_frame.at[row,col] != " "*12:
                    self.data_frame.at[row,col] = 'HOUSA'+ self.data_frame.at[row,col]
                else:
                    self.data_frame.at[row,col] = ' '*5 + self.data_frame.at[row,col]





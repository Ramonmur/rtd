.. _notcoolanimals:

Not Cool Animals Module
=========================

Here is some module level information for not cool animals. To know about cows :ref:`say MOO <coolanimals>`


.. automodule:: not_cool_animals
    :members:
    :undoc-members:
    :show-inheritance:

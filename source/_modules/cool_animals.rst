.. _coolanimals:

Cool Animals Module
====================
Here is some information at module level for cool animals. To visit not cool animals :ref:`click here <notcoolanimals>`


.. automodule:: cool_animals
    :members:
    :show-inheritance:



Modules description:
====================

This document explains briefly the classes and modules from this project and contain links to them.

:ref:`Cool animals <coolanimals>`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Cool animals says moo!!!! there are :class:`Cows <cool_animals.Cow>` and there are :class:`Calves <cool_animals.Calf>`


:ref:`Not cool animals <notcoolanimals>`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Not cool animals don't say moo!! Some of them :class:`say heeee <not_cool_animals.Horses>`


Finally here is a link to a `A MARKDOWN FILE <http://rtdramon123456.readthedocs.io/en/latest/Introduction.html>`_
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


"""
MODULE NOT COOL ANIMALS
========================
This module contain functions related with not cool animals

Links
^^^^^
:module:`<source.cool_animals>`

Other tipe of link :module:`Click here to visit cool_animals module <source.cool_animals>`

"""






import pandas as pd
import numpy as np


class Horses(object):
    """
    This class is related to :class:`cows <cool_animals.Cow>` but is much worse.

    so don' have any methods, it is just here to join some functions outside classes.
    """


def hunt(self, list_Key_values):
    """
    This function is intended to hunt wild animals.

    :param list_Key_values: (list) List containing the names of the key colums to be checked.

    :return: The data frame with the animals that can be hunted

    Example:

    >>> rm_key_values['Body weight','Age'])
    'Body weight  Age
        100        25
         80        32'
    """
    self.data_frame = self.data_frame.dropna(axis=0, how='any', subset=list_Key_values)



def fish_no_docstrings(self, columns):

        for row in self.data_frame.index:
            for col in [columns]:
                if self.data_frame.at[row,col] != " "*12:
                    self.data_frame.at[row,col] = 'HOUSA'+ self.data_frame.at[row,col]
                else:
                    self.data_frame.at[row,col] = ' '*5 + self.data_frame.at[row,col]

